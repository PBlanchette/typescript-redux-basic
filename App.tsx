/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';

import {SafeAreaView,}  from 'react-native';


import Dummy from './Dummy';
import { Provider } from 'react-redux';

import {Store, createStore } from 'redux';

import { DummyRootState } from './Dummy';
import { types } from '@babel/core';
import { combineReducers } from 'redux';

export const initialState:DummyRootState = {
  propFromStore:"store init Value"
}

export const SET_PROP_FROM_STORE = "SET_PROP_FROM_STORE"

interface setPropFromStoreAction {
  type: typeof SET_PROP_FROM_STORE
  payload:string
}

type Actions = setPropFromStoreAction

function onSetPropFromStore (newProp:string): Actions {
  return {
    type:SET_PROP_FROM_STORE,
    payload:newProp
  }
}

function testPropReducer(state=initialState,action:Actions): DummyRootState{
  switch(action.type){
    case SET_PROP_FROM_STORE:
      return {
        propFromStore: action.payload
      }
    default:
      return state
  }
}

const rootReducer = combineReducers({testPropReducer})

//why this ? ReturnType will infer the state of the root reducer, the new rootstate interface will be user where ? i dunnooo
type RootState = ReturnType<typeof rootReducer>
const megastore = createStore(rootReducer)

const App = () => {

  return (
    <Provider store={megastore}>
      <SafeAreaView>
        <Dummy propFromParent="passed dummy props"/>
      </SafeAreaView>
    </Provider>
  );
};

export default App;
