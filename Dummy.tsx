import React from 'react';
import { Component } from 'react';
import { View, Text } from 'react-native';

import { connect, ConnectedProps, Provider } from 'react-redux';

interface OwnProps {
  propFromParent:string
}

export interface DummyRootState {
  propFromStore:string
}

const mapState = (state:DummyRootState) => ({
  //LA LIGNE QUI TUE,
  propFromStore: state.testPropReducer.propFromStore

  //avant c'était:
  //propFromStore: state.propFromStore
})

const connector = connect(mapState)
type PropsFromRedux = ConnectedProps<typeof connector>

type Props = PropsFromRedux & OwnProps

class Dummy extends Component<Props> {

  static defaultProps = {
    propFromParent: "default prop"
  }

  constructor (props:Props){
    super(props)
  }

  render(){

    return (
        <View>
          <Text>Dummy from props !  : {this.props.propFromParent}</Text>     
          <Text>prop from store !  : {this.props.propFromStore}</Text>     
        </View>
    )
  } 
}

export default connector(Dummy)

